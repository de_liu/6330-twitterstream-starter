'''
    in this example, write the parsed tweets and user info to mysql. 
    to create the necessary database tables, execute twitter_ddl.sql first.

    we use pylib.simplemysql, a modified version of a pylib library, to interact with MySQL

todo:
1.  Observe the new global variable _writedb_ and its purpose
2.  Uncomment the "#connect to db" block, and the #disconnect database block,
    Uncomment the db.end() in break_handler
    these allow you to configure db connection and disconnect from it after use.
3.  Observe the "output user_data" block. 
    Try offline debugging with DB - read from tweet_normal.txt
4.  Mimic "user_data output", write code for outputting 
    tweet_data. Test it offline first, then online.

'''

import config #put config.py in the same directory
import json
import sys
import tweepy #needs installation
from pylib import utils
from pylib.simplemysql import SimpleMysql

_debug_ = True #If true, on error, save to local file and stop. otherwise, ignore errors
_online_ = True # If true, read from online sources. Otherwise, from a local file
_writedb_ = False # If true, write outputs to database. Otherwise, print on screen.

'''
suggested debugging sequence:
[1] (offline debugging w/o db) set _debug_ to True, _online_ to False, _writedb_ to False: 
    Test on a local tweet first to see if there is any error
[2] (offline debugging w/ db) set _debug_ to True, _online_ to False, _writedb_ to True: 
    so that you can succesefully write results to DB
[3] (online debugging w/o db) set _debug_ to True, _online_ to True, _writedb_ to False: 
    you can test there are errors in processing streaming data 
    if there are errors, you save the error tweet to a local file
[4] (production) set _debug_ to False, _online_ to True, _writedb_ to True:
    ignore the remaining errors and write results to DB.     


'''

# the keys in the raw tweets are not the same
# as the fields in the database. we use this
# to map between the two and indicate which field to extract
# the format is: source_dict_key: db_field

tweet_fields_map = {
    'id':'tweet_id',
    'text':'tweet_text',
    'source':'source',
    'created_at':'created_at',
    'retweet_count':'retweet_count',    
    'in_reply_to_status_id':'reply_tweet_id',
    'in_reply_to_user_id':'reply_user_id',
    'favorite_count':'favorite_count',
}

#for users table
user_fields_map = {
    'name':'name',
    'screen_name':'screen_name',
    'id':'user_id',
}

def get_fields(source, fields_map):
    '''
        given a source dict, extract fields in the fields_map and save to a destination dict 
        and rename keys according to fields_map. e.g. fields_map['name']='user_name' 
        would get 'name' in source dict and save in the 'user_name' of the destination dict. 
    '''
    data={}    
    if isinstance(source, dict):
        for source_field, dest_field in fields_map.iteritems():
            if source.get(source_field):
                data[dest_field]=source[source_field]
        return data
    else:
        print "source is not a dict:" + type(source)
    
class CustomStreamListener(tweepy.StreamListener):

    def on_data(self, status):
        #on_data provides data in the raw json string
        try:    
            #record the time of event
            capture_date = utils.mysql_time()
            
            tweet = json.loads(status)
            if tweet.get('delete'):
                return True ## skip delete notices
            # if tweet.get('lang') != 'en':
                #return True ## skip non-English tweets    
                
            tweet_data = {}
            user_data = {}
            
            # extract tweet fields and rename to db fields    
            tweet_data.update(get_fields(tweet,tweet_fields_map))
            
            # error checking
            if not tweet_data.get('tweet_id'):
                print('No tweet id')
                if _debug_:
                    #save the tweet for debugging
                    utils.dump_json(tweet,'debug/tweet.txt')
                    return False
                else:
                    return True  #discard this tweet 
            
            #futher process fields that need special processing
            if tweet_data.get('created_at'):
                #convert to mysql date time format.
                tweet_data['created_at']=utils.utc_to_mysql_time(tweet_data['created_at'])
                    
            if tweet.get('user'):
                # retrieve most of user fields.
                user_data.update(get_fields(tweet['user'],user_fields_map))    
                if not user_data.get('user_id'):
                    print('No tweet user id')
                    if _debug_:
                        utils.dump_json(tweet,'debug/tweet%s.txt'%tweet_data.get('tweet_id'))
                        return False
                    else:
                        return True  #discard this tweet         

            #enrichment    
            if tweet_data: 
                tweet_data['user_id'] = user_data.get('user_id')
                tweet_data['capture_date']=capture_date
            if user_data:
                user_data['capture_date']=capture_date
         
            print "%s tweet id:%s"%(capture_date,tweet_data.get('tweet_id'))
            
            #output user_data
            #we do this first because in MySQL user_id must be inserted before tweet_id.
            if user_data: 
                if _writedb_:
                    #insert to db and print a message
                    print "%i user inserted:%s"%(db.insert('users', user_data),user_data['name'])
                else:
                    print "user_data"
                    print json.dumps(user_data,indent=2)
                    print     

            #output tweet_data    
   
            print

         
        except Exception, e:
            if _debug_:
                #write the tweet to local disk for debugging
                utils.dump_json(status,'debug/tweet.txt')
                raise 
            else:
                pass #ignore errors
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True     #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listening

if __name__ == '__main__':

    #create a listener object
    listener = CustomStreamListener()

    utils.enable_utf_print()  #so that unicode characters can be printed on screen.

    def break_handler(signal, frame):
        '''what to do when ctrl+c is pressed
        '''
        print 'You pressed Ctrl+C!'
        try:
            stream.disconnect()
            #db.end()
        except Exception, e:
            pass
        print 'Bye!'
        sys.exit(0)    

    utils.handle_break(break_handler) # so that when control c is pressed, sys will exit
    
    #connect to db
    # if _writedb_:
    #     # configure db connection
    #     db = SimpleMysql(
    #         host=config.db_host,
    #         db=config.db_db,
    #         user=config.db_user,
    #         passwd=config.db_passwd,
    #         keep_alive=True # try and reconnect timedout mysql connections?
    #     )

    if _online_:    
        #create an auth object
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.oauth_token, config.oauth_token_secret)
            
        #attach the listener object to twitter stream
        stream = tweepy.streaming.Stream(auth, listener)
        
        #you may choose to track certain topics
        stream.filter(track=['bigdata',"big data","data science"])    
        
        #or receive a sample of all tweets - that's a lot!
        # stream.sample()

        while True:
            if stream.running is False:
                print 'Stream stopped!'
                break
            time.sleep(1) #sleep for 1 sec
                
        stream.disconnect()
        print 'Bye!'
    else:
        print "-- offline mode --"
        print ""
        # read and parse locally stored tweet
        #listener.on_data(utils.read_file('debug/tweet_jp.txt'))
        #listener.on_data(utils.read_file('debug/tweet_delete.txt'))
        listener.on_data(utils.read_file('debug/tweet_normal.txt'))
        #listener.on_data(utils.read_file('debug/tweet_example_entities.txt'))
    
    #disconnect database
    # if _writedb_:
    #     db.end()
