'''
	test configuration.
	
'''

import config #put config.py in the same directory
import json
import tweepy #needs installation
from pylib import utils
from pylib.simplemysql import SimpleMysql

if __name__ == '__main__':
    print "You're expected to see printout of SimpleMysql and Tweepy API instances"
    print "if you do not see error messages, then you're ready to go"
    print 
    #test db configuration.
    db = SimpleMysql(
        host=config.db_host,
        db=config.db_db,
        user=config.db_user,
        passwd=config.db_passwd,
        keep_alive=True
    )
    
    db.getOne("tweets") #select one row from table tweets
    
    # test twitter configuration
    auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
    auth.set_access_token(config.oauth_token, config.oauth_token_secret)
    
    api = tweepy.API(auth)
    api = tweepy.API(auth)
    tweets = api.user_timeline(id="CarlsonNews") #receive timeline of user carlsonnews
    