'''
    in this example, we do some deeper parsing,
    extract useful information in a tweet into dict objects

Todos:
1.  Read and understand the tweet_fields_map and user_fields_map
    and the function get_fields
2.  Observe how we extract twitter data fields.
3.  Write similar script to extract user data fields.
3.  Add script for converting UTC time to MySQL time for created_at.
4.  Uncomment capture_date = utils.mysql_time(), and tweet_data enrichment lines.
    Mimic this to write enrichment of user_data by adding capture_date
5.  Enhance break handling by uncommenting break_handler and supply this function to handle_break
    as an argument.
    

'''

import config #put config.py in the same directory
import json
import sys
import tweepy #needs installation
from pylib import utils

_debug_ = True #If true, on error, save to local file and stop. otherwise, ignore errors
_online_ = True # If true, read from online sources. Otherwise, from a local file

'''
suggested debugging sequence:
[1] (offline) start with True, False, so that you can test on a local tweet first.
[2] (online, debug) use True, True, so that you can test there are errors in processing 
    streaming data if there are errors, you get the error tweet saved to one of the local files.
    rename the file so that it won't get overridden. 
[3] (online, no_debug) False, True: problematic tweets will be ignored
'''


# the keys in the raw tweets are not the same
# as the fields in the database. we use this
# to map between the two and indicate which field to extract
# the format is: source_dict_key: db_field

tweet_fields_map = {
    'id':'tweet_id',
    'text':'tweet_text',
    'source':'source',
    'created_at':'created_at',
    'retweet_count':'retweet_count',    
    'in_reply_to_status_id':'reply_tweet_id',
    'in_reply_to_user_id':'reply_user_id',
    'favorite_count':'favorite_count',
}

#for users table
user_fields_map = {
    'name':'name',
    'screen_name':'screen_name',
    'id':'user_id',
}

def get_fields(source, fields_map):
    '''
        given a source dict, extract fields in the fields_map and save to a destination dict 
        and rename keys according to fields_map. e.g. fields_map['name']='user_name' 
        would get 'name' in source dict and save in the 'user_name' of the destination dict. 
    '''
    data={}    
    if isinstance(source, dict):
        for source_field, dest_field in fields_map.iteritems():
            if source.get(source_field):
                data[dest_field]=source[source_field]
        return data
    else:
        print "source is not a dict:" + type(source)

class CustomStreamListener(tweepy.StreamListener):

    def on_data(self, status):
        #on_data provides data in the raw json string
        try:    
            #record the time of event
            # capture_date = utils.mysql_time()
            
            tweet = json.loads(status)
            if tweet.get('delete'):
                return True ## skip delete tweet
            # if tweet.get('lang') != 'en':
                # return False    
                
            tweet_data = {}
            user_data = {}
            
            # extract tweet fields and rename to db fields    
            tweet_data.update(get_fields(tweet,tweet_fields_map))
            
            if not tweet_data.get('tweet_id'):
                print('No tweet id')
                if _debug_:
                    #save the tweet for debugging
                    utils.dump_json(tweet,'debug/tweet.txt')
                    return False
                else:
                    return True  #discard this tweet 

            # if user key exists, extract user fields and test existance of user_id 
            # on error, save to debug/tweet[tweetid].txt eg. tweet232234083222.txt
            
            #futher process fields that need special processing
            #convert created_at to mysql date time format with utils.utc_to_mysql_time
          
            #enrichment    
            if tweet_data: 
                # tweet_data['user_id'] = user_data.get('user_id')
                # tweet_data['capture_date']=capture_date
                print "tweet_data"
                print json.dumps(tweet_data,indent=2)
                print

            # if user_data exists, add capture_date and print dict.

        except Exception, e:
            if _debug_:
                #write the tweet to local disk for debugging
                utils.dump_str(status,'debug/tweet.txt')
                raise 
            else:
                pass #ignore errors
    def on_error(self, status_code):
        print "Got an API error with status code %s" % str(status_code)
        return True     #continue to listen
    def on_timeout(self):
        print "Timeout..."
        return True  #continue to listening

if __name__ == '__main__':

    #create a listener object
    listener = CustomStreamListener()

    utils.enable_utf_print()  #so that unicode characters can be printed on screen.
    
    # def break_handler(signal, frame):
    #     '''what to do when ctrl+c is pressed
    #     '''
    #     print 'You pressed Ctrl+C!'
    #     try:
    #         stream.disconnect()
    #     except Exception, e:
    #         pass
    #     print 'Bye!'
    #     sys.exit(0)    

    utils.handle_break() # so that when control c is pressed, sys will exit

    if _online_:    
        #create an auth object
        auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
        auth.set_access_token(config.oauth_token, config.oauth_token_secret)
            
        #attach the listener object to twitter stream
        stream = tweepy.streaming.Stream(auth, listener)
        
        #you may choose to track certain topics
        stream.filter(track=['bigdata',"big data","data science"])  
        
        #or receive a sample of all tweets - that's a lot!
        #stream.sample()

        while True:
            if stream.running is False:
                print 'Stream stopped!'
                break
            time.sleep(1) #sleep for 1 sec
                
        stream.disconnect()
        print 'Bye!'
    else:
        print "-- offline mode --"
        print ""
        #listener.on_data(utils.read_file('debug/tweet_jp.txt'))
        #listener.on_data(utils.read_file('debug/tweet_delete.txt'))
        listener.on_data(utils.read_file('debug/tweet_example_entities.txt'))
